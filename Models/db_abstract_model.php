<?php 
/*
Creado por wqinmz el 06/11/2017. Modelo de datos PHP realizado mediante una clase abstracta que contiene los métodos necesarios para
conectarse a la base de datos y realizar operaciones sobre la misma. Los métodos para operar sobre cada una de las tablas concretas
serán implementados por clases no abstractas que heredarán DBAbstractModel.
*/
	abstract class DBAbstractModel { 
		#Propiedades
    	private static $db_host = 'localhost'; 				//Host donde está ubicada la base de datos.    
		private static $db_user = 'useriu';     			//Usuario necesario para conectarse al servidor de la Base de datos.
		private static $db_pass = 'passiu';     			//Contraseña necesaria para autentificarse.
		protected $db_name = 'IU2';     					//Nombre de la base de datos.
		protected $query;     								//Almacenará las sentencias SQL antes de ser ejecutadas.
		protected $rows = array();     						//Almacenará los resultados de las consultas que se realicen.
		protected $conn;     								//Variable que controla la conexión con el servidor de la base de datos.
		public $mensaje = true; 							
		# Métodos abstractos, serán implementados por las clases que la hereden
        abstract protected function get(); 					//Método abstracto mediante el cual se obtendendrán datos de la DB
		abstract protected function set();					//Método abstracto mediante el cual se insertarán datos en la DB
		abstract protected function edit();					//Método abstracto mediante el cual se modificarán datos ya presentes en la DB.
		abstract protected function delete();				//Método abstracto mediante el cual se eliminarán datos de la DB.
		# Los siguientes métodos no son abstractos dado que su implementación será identica en las clases que hereden
		//Método de acceso público para abrir conexión con la DB.
		public function open_connection() {
			$this->conn = mysqli_connect(self::$db_host, self::$db_user, self::$db_pass, $this->db_name);  
			if(!$this->conn){														//Si no se pudo conectar con la DB, lanzar un mensaje de error.
				echo "<script> alert(\"No se puede conectar a la Base de Datos\");</script>";
				exit;
			}
		} 
		//Método de acceso público para cerrar conexión con la DB.
		public function close_connection() {
			 $this->conn->close(); 
		} 
		//Método de acceso protegido para ejecutar querys que no devuelvan datos (INSERT, UPDATE, DELETE, REPLACE).
		protected function execute_single_query() {     
			if($_POST) {         
				$this->open_connection();      												//Abrir conexión con la DB.
				$this->conn->query($this->query); 											//Ejecutar la query
				$this->mensaje = mysqli_errno($this->conn);
				$this->close_connection();     												//Cerrar conexión con la DB
			} 
			else {         
				$this->mensaje = 'Metodo no permitido';     								
			} 
		} 
		# Llevar a cabo una consulta y guardar sus resultados en un Array ($this->rows).
		protected function get_results_from_query() {
		         $this->open_connection();         											//Abrir la conexión con la DB.
				 $result = $this->conn->query($this->query);         						//Guardar tuplas obtenidas en result.
				 while ($this->rows[] = $result->fetch_assoc());							//Mientras haya tuplas, va guardando cada una como array en rows.
				          $result->close();
						  $this->close_connection();
						  array_pop($this->rows); 
		} 
	} 
?>
