<?php
/*
Archivo creado por wqinmz el 08/11/2017. Clase que contiene el modelo de datos de la tabla de DB Usuario y los métodos necesarios para manejarla.
*/
	require 'db_abstract_model.php';
	class Usuario extends DBAbstractModel {
		#Propiedades
		public $login; 						//Nick del usuario empleado por el sistema para logear
		private $password; 					//Contraseña
		public $dni; 						//Número nacional de identidad
		public $nombre; 					//Nombre del usuario
		public $apellidos; 					//Apellidos del usuario
		public $telefono; 					//Teléfono del usuario
		public $email; 						//Correo electrónico del usuario
		public $fechaNacimiento; 			//Fecha de nacimiento del usuario
		public $fotoPersonal; 				//Foto del usuario empleada como avatar (enlace?)
		public $sexo; 						//Sexo del usuario
		
		#Métodos
		
		// constructor de la clase
		// se inicializa con los valores de los atributos de la tabla
		function __construct($login,$password,$dni,$nombre,$apellidos,$telefono,$email,$fechaNacimiento,$fotoPersonal,$sexo) {
			$this->login = $login;
			$this->password = $password;
			$this->dni = $dni;
			$this->nombre = $nombre;
			$this->apellidos = $apellidos;
			$this->telefono = $telefono;
			$this->email = $email;
			$this->fechaNacimiento = $fechaNacimiento;
			$this->fotoPersonal = $fotoPersonal;
			$this->sexo = $sexo;
		}
		/*
		Método que comprueba si los datos son válidos y se ajustan a la base de datos. En caso de no ser así, devuelve un error.
		*/
		public function validacion(){
			$this->query = "SELECT *
					FROM USUARIO
					WHERE (login = \"$this->login\");";	
			$this->get_results_from_query();
			
			if(count($this->rows)>0){						//Comprueba si existe el login
				return 'Ese login ya existe.';
			}
			
			$this->query = "SELECT *
					FROM USUARIO
					WHERE (DNI = \"$this->dni\");";	
			$this->get_results_from_query();
			
			if(count($this->rows)>0){						//Comprueba si existe el DNI
				return 'Ese DNI ya está registrado.';
			}
			
			$this->query = "SELECT *
					FROM USUARIO
					WHERE (email = \"$this->email\");";	
			$this->get_results_from_query();
			
			if(count($this->rows)>0){						//Comprueba si existe el correo electrónico
				return 'Ese correo electrónico ya está registrado';
			}
			
			if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {				//Comprueba si el mail tiene un formato correcto
				return 'Esa dirección de correo electrónica no es válida';
			}
			
			if(!preg_match('/^(\+34|0034|34)?[0-9]{9}$/', $this->dni)){			//Comprueba si el telefono tiene un formato correcto
				return 'El teléfono introducido no es válido.';
			}
			
			$letra = substr($this->dni, -1);			//Almacena la letra del DNI
			$numeros = substr($this->dni, 0, -1);		//Almacena los números del DNI
			//Comprueba si el DNI tiene un formato correcto
			if (!substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
				return 'El DNI introducido no es válido';
			}
			
			if($this->sexo!='hombre'||$this->sexo!='mujer'){	//Comprueba que el campo sexo contiene un dato válido
				return 'Sexo inválido';
			}
			
			return true;
		}
		
		/*
		Método de ADD. Inserta una tupla válida.
		*/
		public function set(){		 
			if($this->validacion()!=true){ //Comprueba que todos los campos son válidos y devuelve un error si no es así
				return $this->validacion();
			}
			else{
				$this->query = "INSERT INTO `USUARIO` 
					(`login`, `password`, `DNI`, `nombre`, `apellidos`, `telefono`, `email`, `FechaNacimiento`, `fotopersonal`, `sexo`)
					VALUES 
					(\"$this->login\",
					 \"$this->password\",
					 \"$this->dni\",
					 \"$this->nombre\",
					 \"$this->apellidos\",
					 \"$this->telefono\",
					 \"$this->email\", 
					 \"$this->fechaNacimiento\",
					 \"$this->fotoPersonal\",
					 \"$this->sexo\");";
				$this->execute_single_query();
				return $this->mensaje;
			}
		}
		
		//Método de SEARCH. Devuelve un array de tuplas con las propiedades del objeto traído de DB
		public function get() {
			$this->query = "
		        	SELECT      *
					FROM        `USUARIO`
					WHERE       
					`login` like '$this->login' AND
					`password` like '$this->password' AND
					`DNI` like '$this->dni' AND
					`nombre` like '$this->nombre' AND
					`apellidos` like '$this->apellidos' AND
					`telefono` like '$this->telefono' AND
					`email` like '$this->email' AND
					`FechaNacimiento` like '$this->fechaNacimiento' AND
					`fotopersonal` like '$this->fotoPersonal' AND
					`sexo` like '$this->sexo';
					";
			$this->get_results_from_query();
			return $this->rows;
		}
		
		//Devuelve la primera tupla de un array obtenido con get()
		public function loadUser(){
			$userDB = $this->get();
			foreach ($userDB as $row) {
				return new Usuario($row["login"],
				$row["password"],
				$row["DNI"],
				$row["nombre"],
				$row["apellidos"],
				$row["telefono"],
				$row["email"],
				$row["FechaNacimiento"],
				$row["fotopersonal"],
				$row["sexo"]);
			}
		}
		
		/*
		Método de EDIT. Modifica una tupla con información válida.
		*/
		public function edit() {
			if($this->validacion()!=true){ //Comprueba que todos los campos son válidos y devuelve un error si no es así
				return $this->validacion();
			}
			else{
				$this->query = "UPDATE `USUARIO` 
					SET 
					`password` = \"$this->password\", 
					`DNI` = \"$this->dni\", 
					`nombre` = \"$this->nombre\", 
					`apellidos` = \"$this->apellidos\", 
					`telefono` = \"$this->telefono\", 
					`email` = \"$this->email\", 
					`FechaNacimiento` = \"$this->fechaNacimiento\", 
					`fotopersonal` = \"$this->fotoPersonal\", 
					`sexo` = \"$this->sexo\"
					WHERE `login` = \"$this->login\";";
				$this->execute_single_query(); //Ejecuta la query y devuelve el mensaje de error de haberlo
				return $this->mensaje; 
			}
		}
		
		/*
		Método de DELETE. Borra la tupla con el login especificado en las propiedades.
		*/
		public function delete() {
			$this->query = "DELETE FROM `USUARIO` WHERE `login` = '$this->login';";
			$this->execute_single_query();
			return $this->mensaje; 
		}
		
		/*
		Método de Logeo. Comprueba si los datos introducidos existen en DB y son válidos.
		*/
		public function login(){
			$this->query = "SELECT *
					FROM USUARIO
					WHERE login = \"$this->login\";";
					
			$this->open_connection(); 
			$resultado = $this->conn->query($this->query);
			$this->close_connection(); 
			
			if ($resultado->num_rows == 0){
				return 'El nombre de usuario introducido no existe.';
			}
			else{
				$tupla = $resultado->fetch_array();
				if ($tupla['password'] == $this->password){
					return true;
				}
				else{
					return 'La contraseña introducida no es correcta.';
				}
			}
		}
	}	
?>