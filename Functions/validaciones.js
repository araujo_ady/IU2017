/* Javascript con funciones de validación de campos. Autor: Adrián Araujo Gregorio. Fecha: 06/10/2017 */

	/* 
	Comprueba si el campo especificado por parámetro está vacío.
	*/
	function comprobarVacio(campo)
	{
		if ((campo.value == null) || (campo.value.length == 0)){ 			//Si el campo está vacío, devuelve false
			campo.style.borderColor = "red";								//El campo se vuelve amarillo
  			campo.focus();	
  			return false;
  		}
  		else{ 																//Si el campo no está vacío, devuelve true.
			campo.style.borderColor = "green";							//El campo se vuelve verde
  			return true;
  		}
	}

	/*
	Comprueba que el texto introducido en el campo que se le pasa por parámetro es de longitud inferior al valor especificado por size. 
	*/
	function comprobarTexto(campo,size)
	{
		if (campo.value.length>size) { 										//Si el campo es mayor que size, devuelve false.
        	campo.style.borderColor = "red";							//El campo se vuelve rojo
			campo.focus();
    		return false;
  		}
		campo.style.borderColor = "green";								//El campo se vuelve verde
  		return true;
  	}
	
	/*
	Comprueba si el contenido del campo pasado por parámetro tiene una longitud inferior a la especificada por el parámetro size y si es una
	cadena formada unica y exclusivamente por caracteres alfabéticos.
	*/
	function comprobarAlfabetico(campo, size)
	{
		if(!comprobarTexto(campo, size)) //Comprueba que la longitud del contenido del campo es inferior a size.
		{
			campo.style.borderColor = "red";							//El campo se vuelve rojo
			return false; //Si supera a size, devuelve false.
		}
		
		var texto = campo.value; //Almacena el texto introducido en el campo que se pasa por parámetro
		var expresion_regular_alfabet = /^[a-zA-Z]+$/; //Expresión regular que se cumple si es una cadena de letras mayusculas o minusculas
		
		if(!expresion_regular_alfabet.test(texto)) //Evalua si el contenido de texto está formado unicamente por caracteres alfabeticos
		{
			campo.style.borderColor = "red";							//El campo se vuelve rojo
			return false;
		}
		
		campo.focus();
		campo.style.borderColor = "green";								//El campo se vuelve verde
		return true;
		
	}
	
	/*
	Comprueba que el valor especificado en el campo que se le pasa por parámetro es un entero y está entre los parámetros 			valormenor y valormayor
	*/
	function comprobarEntero(campo, valormenor, valormayor)
	{
		var expresion_regular_ent =/^[0-9]+$/; //Almacena la expresión regular para comprobar si unicamente hay caracteres numéricos
		if(!expresion_regular_ent.test(campo.value)) //Comprueba que no haya caracteres no numéricos
		{
			alert('Hay caracteres no numéricos');
			return false;
		}
		
		else if ((campo.value > valormayor) || (campo.value < valormenor)) //Si el valor no está entre los parámetros devuelve false.
		{
    		alert('El número debe estar comprendido entre ' + valormenor + ' y ' + valormayor);
        	campo.focus();
    		return false;
  		}
		
		alert('Correcto!');
  		return true;
	}
	
	/*
	Comprueba que el valor especificado en el campo que se le pasa por parámetro es un real con número de decimales especificado por 
	parámetro y está entre valormenor y valormayor
	*/
	function comprobarReal(campo, numero_decimales, valormenor, valormayor)
	{
		var expresion_regular_real =/^[0-9]+([.][0-9]+)?$/; //Expresión regular de números decimales
		var decimales = 0; //Contador del número de decimales
		var numero = campo.value; //Guarda el número para posterior manipulación.
		if(!expresion_regular_real.test(campo.value)) //Comprueba que no haya caracteres no numéricos
		{
			alert('Hay caracteres no numéricos');
			return false;
		}
		
		else if ((campo.value > valormayor) || (campo.value < valormenor)) //Si el valor no está entre los parámetros devuelve false.
		{
    		alert('El número debe estar comprendido entre ' + valormenor + ' y ' + valormayor);
        	campo.focus();
    		return false;
  		}

		/*
		Multiplica por diez el numero tantas veces como decimales permitidos haya más uno. Si en alguna iteración el modulo
		del numero hayado es cero, el número será válido. En caso contrario, el número tendrá más decimales de lo permitido
		*/
		for(i = 0 /*Contador de iteraciones*/; i < numero_decimales + 1 /*Hasta el nº de decimales pasado por parámetro + 1*/; i++) 
		{
			if(numero%1!=0) //Si el numero no tiene modulo cero, sigue teniendo cifras decimales. Multiplicar por diez.
			{
				numero *= 10;
			}
			else //Si no, el número ya es entero y tendrá menos decimales de los indicados en el parámetro, así que devolver true.
			{
				return true;
			}
		}
		
		alert('No puede haber más de ' + numero_decimales + 'decimales');
		return false;
	}
	
	/*
	Comprueba que el NIF especificado en el campo que se pasa por parámetro cumple con la estructura del NIF español.
	*/
	function comprobarDni(campo)
	{
		if(!comprobarVacio(campo)){											//Si el campo está vacío, devolver false.
			return false;
		}

		var numero; //Almacerá el componente numérico del NIF.
		var let; 	//Almacerá la letra del NIF.
		var letra;	//Variable de comprobación de letra.
		var expresion_regular_dni = /^\d{8}[a-zA-Z]$/; //Expresión regular del NIF, 8 dígitos seguidos de una letra.
	
		dni = campo.value;	//NIF introducido en el campo por el usuario
	
		if(expresion_regular_dni.test(dni) == true){ //Si el nif introducido cumple con la expresión
			numero = dni.substr(0,dni.length-1); //asigna a número los primeros 8 caracteres
			let = dni.substr(dni.length-1, 1); //asigna a let el último caracter 
			numero = numero % 23; //Calcula el modulo de numero
			letra = 'TRWAGMYFPDXBNJZSQVHLCKET'; //Posibles letras correspondientes ordenadas
			letra = letra.substring(numero, numero+1); //Calcula la letra que corresponde al número introducido
			if (letra != let) { //Si la letra del NIF no corresponde con la que le toca, false
				campo.style.borderColor = "red";						//El campo se vuelve rojo
				campo.focus();
				return false;
			}else{ 															//La letra corresponde con la calculada. El NIF es correcto.
				campo.style.borderColor = "green";						//El campo se vuelve verde
				campo.focus();
				return true;
			}
		}else{ 																//Si el NIF no cumple la expresión regular, false
			campo.style.borderColor = "red";							//El campo se vuelve rojo
			campo.focus();
			return false;
		}
	}
	
	/*
	Comprueba si el contenido del campo que se pasa por parámetro es un teléfono español válido o no.
	Son teléfonos españoles válidos aquellos que están formados por 9 caracteres y tienen el prefijo (opcional) 34, +34 o 0034
	*/
	function comprobarTelf(campo)
	{
		if(!comprobarVacio(campo)){											//Si el campo está vacío, devolver false.
			return false;
		}
		
		var expresion_regular_telefono = /^(\+34|0034|34)?[0-9]{9}$/; 		//Expresión regular correspondiente a un nº de teléfono de España
		var telefono = campo.value; 										//Almacena el número de teléfono introducido en el campo que se pasa por parámetro
		
		if(expresion_regular_telefono.test(telefono) == true) 				//Si el teléfono cumple con la expresión regular, es correcto. Fín de la función.
		{
			campo.style.borderColor = "green";							//El campo se vuelve verde
			return true;
		}
		
		campo.focus();
		campo.style.borderColor = "red";								//El campo se vuelve rojo
		return false;
	}
	
	/*
	Comprueba si el texto introducido sigue la estructura de un correo electrónico válido
	*/
	function comprobarEmail(campo,size){
		if(!comprobarTextoNoVacio(campo,size)){								//Si no está rellenado o supera el limite de caracteres
			return false;
		}
		
		var exprMail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;		//Almacena la expresión regular de un correo electronico
		if(!(exprMail.test(campo.value))){
			campo.focus();
			campo.style.borderColor = "red";							//El campo se vuelve rojo
  			return false;
		}
		campo.style.borderColor = "green";								//El campo se vuelve verde
		return true;
	}
	
	/*
	Comprueba que hay algún elemento de la lista que se le pasa por parametro seleccionado.
	*/
	function comprobarLista(campo){
		var indice = campo.selectedIndex;									//Indice de la opción seleccionada de la lista
		if( indice == null || indice == 0 ) {								//Si el indice es nulo o igual a 0, no hay elemento seleccionado. Devuelve falso.
			campo.focus();
  			campo.style.borderColor = "red";							//El campo se vuelve rojo
			return false;
		}
		campo.style.borderColor = "green";								//El campo se vuelve verde
		return true;
	}
	/*
	Comprueba si los dos campos que se le pasan por parámetro tienen el mismo contenido
	*/
	function comprobarCamposIguales(campo1, campo2){
		if(campo1.value != campo2.value){
			campo2.focus();
			campo2.style.borderColor = "red";							//El campo se vuelve rojo
			return false;	
		}
		campo2.style.borderColor = "green";								//El campo se vuelve verde
		return true; 
	}
	
	/*
	Comprueba que la fecha introducida en el campo pasado por parámetro no es superior a la actual
	*/
	function comprobarfecha(campo){
		var fecha = new Date();											//Guarda la fecha actual
		var fechaIntr = Date.parse(campo.value);						//Guarda la fecha pasada por parámetro
		if(fechaIntr>fecha.getTime()){											//Si la fecha introducida es superior a la actual, false
			campo.focus();
			campo.style.borderColor = "red";							//El campo se vuelve rojo
			return false;
		}
		campo.style.borderColor = "green";								//El campo se vuelve verde
		return true;
	}
	
	/*
	Comprueba si el campo ha sido rellenado y tiene una longitud inferior a la especificada
	*/
	function comprobarTextoNoVacio(campo,size)
	{
		if(!comprobarVacio(campo)){											//Si el campo está vacío, devolver false.
			return false;
		}
		return comprobarTexto(campo,size);
  	}
	
	/*
	Comprueba si el campo ha sido rellenado, tiene una longitud inferior a la especificada y solo tiene caracteres alfabéticos
	*/
	function comprobarAlfabeticoNoVacío(campo, size)
	{
		if(!comprobarVacio(campo)){											//Si el campo está vacío, devolver false.
			return false;
		}
		
		return comprobarAlfabetico(campo, size);
		
	}
	
	/*
	Valida todos los campos del formulario de inserción o de edición antes de enviar los datos al servidor.
	*/
	function validarCambio(formulario){
		if(!comprobarTextoNoVacio(this.login,15)){						//Comprobación del campo de login
			return false;
		}
		if(!comprobarTextoNoVacio(this.Password,20)){					//Comprobación del campo de password
			return false;	
		}
		if(!comprobarCamposIguales(this.Password, this.PasswordBis)){	//Comparación de que ambas passwords coinciden
			return false;
		}
		if(!comprobarDni(this.DNI)){									//Comprobación del DNI
			return false;
		}
		if(!comprobarAlfabeticoNoVacío(this.nombre,30)){				//Comprobación del nombre
			return false;
		}
		if(!comprobarTextoNoVacio(this.apellidos,50)){					//Comprobación de los apellidos
			return false;
		}
		if(!comprobarTelf(this.telefono)){								//Comprobación del teléfono
			return false;
		}
		if(!comprobarEmail(this.email,60)){								//Comprobación del email
			return false;
		}
		if(!comprobarfecha(this.nacimiento)){							//Comprobación de fecha
			return false;
		}
		if(!comprobarLista(this.sexo)){									//Comprobación de sexo
			return false;
		}
		encriptar();
		return true;													//Si todas las comprobaciones son true, devolver true
	}
	
	/*
	Valida todos los campos del formulario de búsqueda para descartar búsquedas imposibles.
	*/
	function validarBusqueda(formulario){
		if(!comprobarTexto(this.login,15)){								//Comprobación del campo de login
			return false;
		}
		if(!comprobarTexto(this.Password,20)){							//Comprobación del campo de password
			return false;	
		}
		if(!comprobarTexto(this.DNI,9)){								//Comprobación del DNI
			return false;
		}
		if(!comprobarTexto(this.nombre,30)){							//Comprobación del nombre
			return false;
		}
		if(!comprobarTexto(this.apellidos,50)){							//Comprobación de los apellidos
			return false;
		}
		if(!comprobarTexto(this.telefono,13)){							//Comprobación del teléfono
			return false;
		}
		if(!comprobarTexto(this.email,60)){								//Comprobación del email
			return false;
		}
		if(!comprobarfecha(this.nacimiento)){							//Comprobación de fecha
			return false;
		}
		if(comprobarVacio(this.Password)){								//Si se busca por contraseña, cifrar el dato
			encriptar();
		}
		return true;													//Si todas las comprobaciones son true, devolver true
	}
	
	