<?php 
/*
Archivo creado el 10/11/2017 por wqinmz. Contiene un array con todas las strings de la aplicación en Gallego.
*/
	$strings = 														//Array que almacena las strings
	array(
		//Header
		'es'												=>	'gl',
		'Interfaces de Usuario ET2'							=>	'Interfaces de Usuario ET2',
		'Gestión de Usuarios' 								=>	'Xestión de Usuarios',
		'Regístrate'										=>	'Rexístrate',
		'Identifícate'										=>	'Identifícate',
		'Usuario'											=>	'Usuario',
		//Barra lateral	
		'Mi Usuario'										=>	'O meu usuario',
		'Ver Perfil'										=>	'Ver Perfil',
		'Modificar Perfil'									=>	'Modificar Perfil',
		'Borrar perfil'										=>	'Borrar perfil',
		'Buscar Usuario'									=>	'Buscar Usuario',
		'Crear Usuario'										=>	'Crear Usuario',
		//Footer
		'Creada el 11/11/2017 por wqinmz'					=>	'Creada o 11/11/2017 por wqinmz1',
		//Formularios
		'Usuario'											=>	'Usuario',
		'Contraseña'										=>	'Contrasinal',
		'Repite la contraseña'								=>	'Repite o contrasinal',
		'Nombre'											=>	'Nome',
		'Apellidos'											=>	'Apelidos',
		'Teléfono'											=>	'Teléfono',
		'Email'												=>	'Enderezo Electrónico',
		'Fecha de Nacimiento'								=>	'Data de Nacemento',
		'Foto'												=>	'Foto',
		'Sexo'												=>	'Sexo',
		//Mensajes
		'La imagen excede el tamaño máximo permitido.'		=>	'A imaxe excede o tamano permitido',
		'El formato del archivo es inválido.'				=>	'O formato do arquivo é inválido',
		'Ese login ya existe.'								=>	'Ese login xa existe.',
		'Ese DNI ya está registrado.'						=>	'Ese DNI xa está registrado.',
		'Ese correo electrónico ya está registrado'			=>	'Ese enderezo electrónico xa está registrado',
		'Esa dirección de correo electrónica no es válida'	=>	'Ese enderezo de correo electrónica no es válida',
		'El teléfono introducido no es válido.'				=>	'O teléfono introducido non é válido.',
		'El DNI introducido no es válido'					=>	'O DNI introducido no es válido',
		'Sexo inválido'										=>	'Sexo inválido',
		'No se han obtenido resultados'						=>	'Non se obtiveron resultados',
		'La operación se ha completado con éxito'			=>	'A operación completouse con éxito',
		'El nombre de usuario introducido no existe.'		=>	'O nome de usuario introducido non existe.',
		'La contraseña introducida no es correcta.'			=>	'A contrasinal introducida non é correcta.',
		'ErrorBusqueda'										=>	'Produciuse un erro. O usuario buscado non exite ou a consulta devolve mais dun resultado',
		'Inténtalo de nuevo más tarde.'						=>	'Intentao de novo mais tarde.',
		0													=>	'A operación completouse con éxito',
		1062												=>	'Xa existe un usuario con estes datos. Por favor, comproba o teu Usuario, enderezo electrónico e DNI',
		//Atributos
		'hombre'											=>	'home',
		'mujer'												=>	'muller',
		//Table
		'login'												=>	'Usuario',
		'password'											=>	'Contrasinal',
		'DNI'												=>	'DNI',
		'nombre'											=>	'Nome',
		'apellidos'											=>	'Apelidos',
		'telefono'											=>	'Teléfono',
		'email'												=>	'Enderezo Electrónico',
		'FechaNacimiento'									=>	'Data de Nacemento',
		'fotopersonal'										=>	'Foto',
		'sexo'												=>	'Sexo',
				//Mensajes de bienvenida
		'MensajeBienvenida'									=>	"Benvido o sistema de Xestión de Usuarios, " .$_SESSION['login']. "! <br><br> Podes acceder ás opcións disponibles no menú lateral.<br> Para pechar sesión o cambiar de lenguaxe emprega os botóns da cabeceira.<br> <br>Bo día!",
		'¿Estás seguro de querer borrar este usuario?'		=>	'Está vostede seguro de borrar este usuario?'

	);
?>
