<?php 
/*
Archivo creado el 10/11/2017 por wqinmz. Contiene un array con todas las strings de la aplicación en Ingl-es.
*/
	$strings = 														//Array que almacena las strings
	array(
		//Header
		'es'												=>	'en',
		'Interfaces de Usuario ET2'							=>	'User Interfaces ET2',
		'Gestión de Usuarios' 								=>	'User Management',
		'Regístrate'										=>	'Sign up',
		'Identifícate'										=>	'Login',
		'Usuario'											=>	'User',
		//Barra lateral		
		'Mi Usuario'										=>	'My user',
		'Ver Perfil'										=>	'View profile',
		'Modificar Perfil'									=>	'Edit profile',
		'Borrar perfil'										=>	'Delete profile',
		'Buscar Usuario'									=>	'Search user',
		'Crear Usuario'										=>	'Create account',
		//Footer
		'Creada el 11/11/2017 por wqinmz'					=>	'Created on 11/11/2017 by wqinmz',
		//Formularios
		'Usuario'											=>	'User',
		'Contraseña'										=>	'Password',
		'Repite la contraseña'								=>	'Repeat password',
		'Nombre'											=>	'Name',
		'Apellidos'											=>	'Surnames',
		'Teléfono'											=>	'Phone',
		'Email'												=>	'Email',
		'Fecha de Nacimiento'								=>	'Birthdate',
		'Foto'												=>	'Picture',
		'Sexo'												=>	'Gender',
		//Mensajes
		'La imagen excede el tamaño máximo permitido.'		=>	'The image exceeds the maximum size allowed.',
		'El formato del archivo es inválido.'				=>	'The file format is invalid',
		'Ese usuario ya existe.'							=>	'That user already exists.',
		'Ese DNI ya está registrado.'						=>	'That DNI is already registered.',
		'Ese correo electrónico ya está registrado'			=>	'That email is already registered',
		'Esa dirección de correo electrónica no es válida'	=>	'That email address is not valid',
		'El teléfono introducido no es válido.'				=>	'The phone number entered is not valid.',
		'El DNI introducido no es válido'					=>	'The DNI entered is not valid',
		'Sexo inválido'										=>	'Gender invalid',
		'No se han obtenido resultados'						=>	'No results have been obtained',
		'La operación se ha completado con éxito'			=>	'The operation has been successfully completed',
		'El nombre de usuario introducido no existe.'		=>	'El nombre de usuario introducido no existe.',
		'La contraseña introducida no es correcta.'			=>	'The password entered is not correct.',
		'ErrorBusqueda'										=>	'An error has occurred. The searched user does not exist or the query returns more than one result',
		'Inténtalo de nuevo más tarde.'						=>	'Try again later.',
		0													=>	'The operation has been successfully completed',
		1062												=>	'A user with this data already exists. Please check your User, email and DNI',
		//Atributos
		'hombre'											=>	'male',
		'mujer'												=>	'female',
		//Table
		'login'												=>	'Login',
		'password'											=>	'Password',
		'DNI'												=>	'DNI',
		'nombre'											=>	'Name',
		'apellidos'											=>	'Surname',
		'telefono'											=>	'Phone',
		'email'												=>	'Email',
		'FechaNacimiento'									=>	'Birthdate',
		'fotopersonal'										=>	'Picture',
		'sexo'												=>	'Gender',
		//Mensajes de bienvenida
		'MensajeBienvenida'									=>	"Welcome to the User Management System, " .$_SESSION['login']. "! <br><br> You can access the options available in the side menu.<br> To logout or change language use the buttons of the header<br> <br>Have a good day!",
		'¿Estás seguro de querer borrar este usuario?'		=>	'Are you sure you want to delete this user?'

	);
?>
