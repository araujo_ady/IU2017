<?php 
/*
Archivo creado el 10/11/2017 por wqinmz. Contiene un array con todas las strings de la aplicación en Español.
*/
	$strings = 														//Array que almacena las strings
	array(
		//Header
		'es'												=>	'es',
		'Interfaces de Usuario ET2'							=>	'Interfaces de Usuario ET2',
		'Gestión de Usuarios' 								=>	'Gestión de Usuarios',
		'Regístrate'										=>	'Regístrate',
		'Identifícate'										=>	'Identifícate',
		'Usuario'											=>	'Usuario',
		//Barra lateral
		'Mi Usuario'										=>	'Mi Usuario',
		'Ver Perfil'										=>	'Ver Perfil',
		'Modificar Perfil'									=>	'Modificar Perfil',
		'Borrar perfil'										=>	'Borrar perfil',
		'Buscar Usuario'									=>	'Buscar Usuario',
		'Crear Usuario'										=>	'Crear Usuario',
		//Footer
		'Creada el 11/11/2017 por wqinmz'					=>	'Creada el 11/11/2017 por wqinmz1',
		//Formularios
		'Usuario'											=>	'Usuario',
		'Contraseña'										=>	'Contraseña',
		'Repite la contraseña'								=>	'Repite la contraseña',
		'Nombre'											=>	'Nombre',
		'Apellidos'											=>	'Apellidos',
		'Teléfono'											=>	'Teléfono',
		'Email'												=>	'Correo Electrónico',
		'Fecha de Nacimiento'								=>	'Fecha de Nacimiento',
		'Foto'												=>	'Foto',
		'Sexo'												=>	'Sexo',
		//Mensajes
		'La imagen excede el tamaño máximo permitido.'		=>	'La imagen excede el tamaño máximo permitido.',
		'El formato del archivo es inválido.'				=>	'El formato del archivo es inválido',
		'Ese login ya existe.'								=>	'Ese nombre de usuario ya existe.',
		'Ese DNI ya está registrado.'						=>	'Ese DNI ya está registrado.',
		'Ese correo electrónico ya está registrado'			=>	'Ese correo electrónico ya está registrado',
		'Esa dirección de correo electrónica no es válida'	=>	'Esa dirección de correo electrónica no es válida',
		'El teléfono introducido no es válido.'				=>	'El teléfono introducido no es válido.',
		'El DNI introducido no es válido'					=>	'El DNI introducido no es válido',
		'Sexo inválido'										=>	'Sexo inválido',
		'No se han obtenido resultados'						=>	'No se han obtenido resultados',
		'La operación se ha completado con éxito'			=>	'La operación se ha completado con éxito',
		'El nombre de usuario introducido no existe.'		=>	'El nombre de usuario introducido no existe.',
		'La contraseña introducida no es correcta.'			=>	'La contraseña introducida no es correcta.',
		'ErrorBusqueda'										=>	'Se ha producido un error. El usuario buscado no existe o la consulta devuelve más de un resultado.',
		'Inténtalo de nuevo más tarde.'						=>	'Inténtalo de nuevo más tarde.',
		0													=>	'La operación se ha completado con éxito',
		1062												=>	'Ya existe un usuario con estos datos. Por favor, comprueba tu Usuario, correo electrónico y DNI',
		//Atributos
		'hombre'											=>	'hombre',
		'mujer'												=>	'mujer',
		//Table
		'login'												=>	'Usuario',
		'password'											=>	'Contraseña',
		'DNI'												=>	'DNI',
		'nombre'											=>	'Nombre',
		'apellidos'											=>	'Apellidos',
		'telefono'											=>	'Teléfono',
		'email'												=>	'Correo Electrónico',
		'FechaNacimiento'									=>	'Fecha de Nacimiento',
		'fotopersonal'										=>	'Foto',
		'sexo'												=>	'Sexo',
		//Mensajes
		'MensajeBienvenida'									=>	"¡Bienvenido al sistema de Gestión de Usuarios, " .$_SESSION['login']. "! <br><br> Puedes acceder a las opciones disponibles en el menú lateral.<br> Para cerrar sesión o cambiar de idioma utiliza los botones de la cabecera.<br> <br>¡Pasa un buen día!",
		'¿Estás seguro de querer borrar este usuario?'		=>	'¿Estás seguro de querer borrar este usuario?'
	);
?>
