<?php
/*
Archivo creado por wqinmz el 08/11/2017. Vista para sacar mensajes por pantalla
*/
	class MESSAGE{

		private $string; 	//Indice de la string a mostrar
		private $volver;	//Página a la que volvar
		/*
		Costructor. Inicializa los atributos con la información pasada por parametro desde el controlador y muestra la vista
		*/
		function __construct($string, $volver){	
			$this->string = $string;
			$this->volver = $volver;
			$this->render();
		}
		/*
		Carga la vista
		*/
		function render(){
			include '../Views/Header.php'; 
			include '../Views/Workspace.php';
			echo("<p>");
			echo("<H3>");
			echo($strings[$this->string]);
			echo("</H3>");
			echo("</p>");
			echo("<a href='" . $this->volver . "'>" . "Atrás" . " </a>");
			include '../Views/Footer.php'; 
		}
	}
?>