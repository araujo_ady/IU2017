<?php
/*
Archivo creado por wqinmz el 08/11/2017. Vista del formulario de inserción.
*/
	class Usuario_ADD{
		function __construct(){	
			$this->render();
		}
		/*
		Carga la vista
		*/
		function render(){
			include '../Views/Header.php'; 
			include '../Views/Workspace.php';
?>
			<form id="insert" onSubmit="return validarCambio(this)" method="post" enctype="multipart/form-data" action="../Controllers/Insert_Controller.php">
				  <label id="title"><?php echo($strings['Crear Usuario']); ?></label>
				  <br>
				  <label> <?php echo($strings['Usuario']); ?> <br>
					<input id="login" name="login" maxlength=15 onchange="comprobarTextoNoVacio(this,15)" required type="text" >
				  </label>
				  <br>
				  <label> <?php echo($strings['Contraseña']); ?> <br>
					<input id="Password" name="Password" onchange="comprobarTextoNoVacio(this,20)" required type="password">
				  </label>
				  <br>
				  <label> <?php echo($strings['Repite la contraseña']); ?> <br>
					<input id="PasswordBis" name="PasswordBis" maxlength=20 onchange="comprobarCamposIguales(insert.Password, this)" required type="password">
				  </label>
				  <br>
				  <label> DNI <br>
					<input id="DNI" name="DNI" maxlength=9 onchange="comprobarDni(this)" required type="text"> 
				  </label>
				  <br>
				  <label> <?php echo($strings['Nombre']); ?> <br>
					<input id="nombre" name="nombre" maxlength=30 onchange="comprobarAlfabeticoNoVacío(this,30)" required type="text">
				  </label>
				  <br>
				  <label> <?php echo($strings['Apellidos']); ?> <br>
					<input id="apellidos" name="apellidos" maxlength=50 onchange="comprobarTextoNoVacio(this,50)" required type="text">
				  </label>
				  <br>
				  <label> <?php echo($strings['Teléfono']); ?> <br>
					<input id="telefono" name="telefono" maxlength=13 onchange="comprobarTelf(this)" required type="text"> 
				  </label>
				  <br>
				  <label> <?php echo($strings['Email']); ?> <br>
					<input id="email" name="email" maxlength=60 onchange="comprobarEmail(this,60)" required type="email">
				  </label>
				  <br>
				  <label> <?php echo($strings['Fecha de Nacimiento']); ?> <br>
					<input id="nacimiento" name="nacimiento" required type="date" onchange="comprobarfecha(this)">
				  </label>
				  <br>
				  <label><?php echo($strings['Foto']); ?><br>
					<input id="foto" name="foto" maxlength=50 required type="file" accept="image/png, .jpeg, .jpg">
				  </label>
				  <br>
				  <label for="sexo"><?php echo($strings['Sexo']); ?></label>
				  <br>
				  <select id="sexo" name="sexo" required onchange="comprobarLista(sexo)">
					<option value="" selected></option>
					<option value="hombre"><?php echo($strings['hombre']); ?></option>
					<option value="mujer"><?php echo($strings['mujer']); ?></option>
				  </select>
				  <br>
				  <input id="insertar" name="insertar" value="" type="submit">
				  <input id="limpiar" name="limpiar" value="" type="reset">
				</form>
<?php
				include '../Views/Footer.php';
		}
	}
?>