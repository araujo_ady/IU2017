<?php
	class Usuario_EDIT{
		private $tupla;
		function __construct($tupla){	
			$this->tupla = $tupla;
			$this->render();
		}
		function render(){
			include '../Views/Header.php'; 
			include '../Views/Workspace.php';
			if(count($this->tupla) == 1){
				echo('<form id="editar" onSubmit="return validarCambio(this)" method="post" action="../Controllers/Edit_Controller.php" enctype="multipart/form-data">');
				echo('<label id="title">'.$strings['Modificar Perfil'].'</label>');
				echo('<br>');
				echo('<label> '.$strings['Usuario'].' <br>');
				echo('<input id="login" name="login" maxlength=20 onchange="comprobarTextoNoVacio(this,20)" required type="text" value="'.$this->tupla[0]["login"].'" readonly>');
				echo('</label>');
				echo('<br>');
				echo('<label> '.$strings['Contraseña'].' <br>');
				echo('<input id="Password" name="Password" onchange="comprobarTextoNoVacio(this,20)" required type="password" value="">');
				echo('</label>');
				echo('<br>');
				echo('<label> '.$strings['Repite la contraseña'].' <br>');
				echo('<input id="PasswordBis" name="PasswordBis"  maxlength=20 onchange="comprobarCamposIguales(editar.Password, this)" required type="password" value="">');
				echo('</label>');
				echo('<br>');
				echo('<label> DNI <br>');
				echo('<input id="DNI" name="DNI" maxlength=9 onchange="comprobarDni(this)" required type="text" value="'.$this->tupla[0]["DNI"].'">'); 
				echo('</label>');
				echo('<br>');
				echo('<label> '.$strings['Nombre'].' <br>');
				echo('<input id="nombre" name="nombre" maxlength=30 onchange="comprobarAlfabeticoNoVacío(this,30)" required type="text" value="'.$this->tupla[0]["nombre"].'">');
				echo('</label>');
				echo('<br>');
				echo('<label> '.$strings['Apellidos'].' <br>');
				echo('<input id="apellidos" name="apellidos" maxlength=50 onchange="comprobarTextoNoVacio(this,50)" required type="text" value="'.$this->tupla[0]["apellidos"].'">');
				echo('</label>');
				echo('<br>');
				echo('<label> '.$strings['Teléfono'].' <br>');
				echo('<input id="telefono" name="telefono" maxlength=13 onchange="comprobarTelf(this)" required type="text" value="'.$this->tupla[0]["telefono"].'">');
				echo('</label>');
				echo('<br>');
				echo('<label> '.$strings['Email'].' <br>');
				echo('<input id="email" name="email" maxlength=60 onchange="comprobarEmail(this,60)" required type="email" value="'.$this->tupla[0]["email"].'">');
				echo('</label>');
				echo('<br>');
				echo('<label> '.$strings['Fecha de Nacimiento'].' <br>');
				echo('<input id="nacimiento" name="nacimiento" required type="date" onchange="comprobarfecha(this)" value="'.$this->tupla[0]["FechaNacimiento"].'">');
				echo('</label>');
				echo('<br>');
				echo('<label> '.$strings['Foto'].' <br>');
				echo('<input id="foto" name="foto" maxlength=50 required type="file" value="'.$this->tupla[0]["fotopersonal"].'">');
				echo('</label>');
				echo('<br>');
				echo('<label for="sexo">'.$strings['Sexo'].'</label>');
				echo('<br>');
				echo('<select id="sexo" name="sexo" required onchange="comprobarLista(sexo)">');
				echo('<option value="'.$this->tupla[0]["login"].'" selected> '.$this->tupla[0]["sexo"].' </option>');
				echo('<option value="hombre">'.$strings['hombre'].'</option>');
				echo('<option value="mujer">'.$strings['mujer'].'</option>');
				echo('</select>');
				echo('<br>');
				echo('<input id="editarb" name="editar" value="" type="submit">');
				echo('<input id="limpiar" name="limpiar" value="" type="reset">');
				echo('</form>');
			}
			else {
				echo($strings['ErrorBusqueda']);
				echo($strings['Inténtalo de nuevo más tarde.']);
			}
				include '../Views/Footer.php';
		}
	}
?>