<!-- Archivo creado por wqinmz el 08/11/2017. Opciones del menú lateral para las vistas-->
    <ul>
      <li><a><?php echo($strings['Mi Usuario']); ?></a>
        <ul>
          <li><a href="../Controllers/Showcurrent_Controller.php"><?php echo($strings['Ver Perfil']); ?></a></li>
        </ul>
        <ul>
          <li><a href="../Controllers/Edit_Controller.php"><?php echo($strings['Modificar Perfil']); ?></a></li>
        </ul>
		<ul>
          <li><a href="../Controllers/Delete_Controller.php"><?php echo($strings['Borrar perfil']); ?></a></li>
        </ul>
      </li>
      <li><a href="../Controllers/Search_Controller.php"><?php echo($strings['Buscar Usuario']); ?></a></li>
      <li><a href="../Controllers/Insert_Controller.php"><?php echo($strings['Crear Usuario']); ?></a></li>
    </ul>
