<?php
/*
Archivo creado el 10/11/2017 por wqinmz. Contiene la estructura HTML del area de trabajo y la inclusión condicional del menú lateral.
*/
	include_once '../Functions/Authentication.php';
?>

<div id="contenido">
	<nav id="menu">
    <?php
		if (IsAuthenticated()){
			include_once '../Views/Menu.php';
		}
	?>
	</nav>
	<div class="workspace">