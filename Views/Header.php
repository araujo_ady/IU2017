<?php
/*Archivo creado por wqinmz el 08/11/2017. Formato de la cabecera para las vistas y scripting para mostrar información de logeo*/

	session_start();
	include_once '../Functions/Authentication.php';
	if (!isset($_SESSION['idioma'])) {
		$_SESSION['idioma'] = 'SPANISH';
	}
	include_once '../Locales/Strings_' . $_SESSION['idioma'] . '.php'; 
?>
<html lang="<?php echo($strings['es']) ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo($strings['Interfaces de Usuario ET2']) ?></title>
        <link title="style.css" href="../Views/style.css" type="text/css" rel="stylesheet"/>
        <script type="text/javascript" src="../Functions/validaciones.js"></script>
        <script type="text/javascript" src="../Functions/md5.js"></script> 
        <script type="text/javascript">
            function encriptar(){
              document.getElementById('Password').value = hex_md5(document.getElementById('Password').value);
              return true;
            }
        </script>
    </head>
    
    <body>
    <header>
      <h4 align="center"><?php echo($strings['Interfaces de Usuario ET2']) ?></h4>
      <h3 align="center"><?php echo($strings['Gestión de Usuarios']) ?></h3>
      <?php
            if (IsAuthenticated()){
                echo("<label id='user'>".$strings['Usuario'].":".$_SESSION['login']."</label>");
                echo("<a href='../Functions/Desconectar.php'><button id='logoff'></button></a>");
            }
            else{
                echo("<a href='../Controllers/Registro_Controller.php'><button id='cabecera' class='signup'></button></a>");
                echo("<a href='../Controllers/Login_Controller.php'><button id='cabecera' class='login'></button></a>");
            }
        ?>
      <a href='../Functions/Spanish.php'><button id="language" class="spanish"></button></a>
      <a href='../Functions/English.php'><button id="language" class="english"></button></a>
      <a href='../Functions/Galician.php'><button id="language" class="galician"></button></a>
</header>