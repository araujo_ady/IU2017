<?php
	class Usuario_SEARCH{
		function __construct(){	
			$this->render();
		}
		function render(){
			include '../Views/Header.php'; 
			include '../Views/Workspace.php';
?>
			<form id="busqueda" onSubmit="return validarBusqueda(this)" method="get" action="../Controllers/Search_Controller.php">
				<label id="title"><?php echo($strings['Buscar Usuario']); ?></label>
				<br>
				<label> <?php echo($strings['Usuario']); ?> <br>
					<input id="login" name="login" maxlength=15 onchange="comprobarTexto(this,15)" type="search">
				</label>
				<br>
				<label> <?php echo($strings['Contraseña']); ?> <br>
					<input id="Password" name="Password" onchange="comprobarTexto(this,20)" type="search">
				</label>
				<br>
				<label> DNI <br>
					<input id="DNI" name="DNI" maxlength=9 onchange="comprobarTexto(this,9)" type="search"> 
				</label>
				<br>
				<label> <?php echo($strings['Nombre']); ?> <br>
					<input id="nombre" name="nombre" maxlength=30 onchange="comprobarTexto(this,30)" type="search">
				</label>
				<br>
				<label> <?php echo($strings['Apellidos']); ?> <br>
					<input id="apellidos" name="apellidos" maxlength=50 onchange="comprobarTexto(this,50)" type="search">
				</label>
				<br>
				<label> <?php echo($strings['Teléfono']); ?> <br>
					<input id="telefono" name="telefono" maxlength=13 onchange="comprobarTexto(this,13)" type="search"> 
				</label>
				<br>
				<label> <?php echo($strings['Email']); ?> <br>
					<input id="email" name="email" maxlength=60 onchange="comprobarTexto(this,60)" type="search">
				</label>
				<br>
				<label> <?php echo($strings['Fecha de Nacimiento']); ?> <br>
					<input id="nacimiento" name="nacimiento" type="date" onchange="comprobarfecha(this)">
				</label>
				<br>
				<label> <?php echo($strings['Foto']); ?> <br>
					<input id="foto" name="foto" maxlength=50 type="search">
				</label>
				<br>
				<label for="sexo"><?php echo($strings['Sexo']); ?></label>
				<br>
				<select id="sexo" name="sexo">
					<option value="" selected></option>
					<option value="hombre"><?php echo($strings['hombre']); ?></option>
					<option value="mujer"><?php echo($strings['mujer']); ?></option>
				</select>
				<br>
				<input id="buscar" type="submit" name="buscar" value="" />
				<input id="limpiar" type="reset" name="limpiar" value="" />
				</form>
<?php
				include '../Views/Footer.php';
		}
	}
?>