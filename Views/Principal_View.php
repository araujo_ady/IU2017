<?php
/*
Archivo creado por wqinmz el 08/11/2017. Vista de bienvenida.
*/
	class Principal{
		/*
		Constructor. Llama al método que carga la vista.
		*/
		function __construct(){		
			$this->render();
		}
		/*
		Método de carga de la vista
		*/
		function render(){
			include '../Views/Header.php'; 
			include '../Views/Workspace.php';
?>
			<p>
<?php		
			echo($strings['MensajeBienvenida']);
?>
			</p>
<?php
			include '../Views/Footer.php'; 
		}
	}
?>