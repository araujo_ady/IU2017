<?php
/*
Vista Showcurrent. 11/11/2017. Autor: wqinmz. Contiene la vista de tupla en detalle.
*/
	class Usuario_SHOWCURRENT{
		private $tupla;							//Tuplas devueltas por la consulta.
		/*
		Constructor. Inicializa la variable tupla a partir de información que se le pasa desde el controlador y inicia la vista.
		*/
		function __construct($tupla){	
			$this->tupla = $tupla;
			$this->render();
		}
		/*
		Muestra la vista por pantalla
		*/
		function render(){
			include '../Views/Header.php'; 					//Carga la cabecera
			include '../Views/Workspace.php';				//Carga el espacio de trabajo
			$numRes = count($this->tupla);					//Número de tuplas contenidas en $tupla
			if($numRes == 1){								//Si solo hay una tupla, carga la vista
				echo('<HTML>');
				echo('<table id="showcurrent">');
				while($atributo = current($this->tupla[0])){	//Recorrer los elementos de la tupla
					echo('<tr>');
					echo('<th>'.$strings[key($this->tupla[0])].'</th>');
					if(key($this->tupla[0]) == 'fotopersonal'){		//Si el atributo es la foto, convertirla en un enlace
						echo('<td><a href="../Files/'.$atributo.'">Foto personal</a></td>');
					}
					else{										
						if(key($this->tupla[0]) == 'sexo'){	//Si no, comprobar si es sexo para aplicar las traducciones
							echo('<td>'.$strings[$atributo].'</td>');
						}
						else{								//Si no, representar directamente el atributo de la base de datos.	
							echo('<td>'.$atributo.'</td>');
						}
					}
					echo('</tr>');
					next($this->tupla[0]);
				}
				echo('</table>');
			}
			else {									//Si más de una tupla o ninguna, saca un mensaje de error
				echo($strings['ErrorBusqueda']);
			}
				include '../Views/Footer.php';		//Carga el pie de la página
		}
	}
?>