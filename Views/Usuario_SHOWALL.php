
<?php
/*
Archivo creado por wqinmz el 08/11/2017. Script controlador para squedas en la tabla Usuario.

*/
	class Showall{
		private $results;	//Almacena las tuplas obtenidas como resultado
		private $indexes;	//Almacena el nombre de los atributos de la tabla
		/*
		Constructor. Inicializa las variables con los parámetros que se le pasan desde el controlador y carga la vista
		*/
		function __construct($results, $indexes){	
			$this->results = $results;
			$this->indexes = $indexes;
			$this->render();
		}
		function render(){
			include '../Views/Header.php'; 
			include '../Views/Workspace.php';
			$numRes = count($this->results);
			if($numRes > 0){				//Si el número de tuplas es superior a 0 carga la vista
				echo('<HTML>');
				echo('<table id="showall">');
				echo('<tr>');
				while($atributo = current($this->indexes)){	//Recorre el array construyendo la cabecera de la tabla
					echo('<th>'.$strings[$atributo].'</th>');
					next($this->indexes);
				}
				echo('</tr>');
				
				foreach ($this->results as $row) {			//Recorre el array de tuplas
					echo('<tr>');
					while($valor = current($row)){			//Recorre las tuplas representandolas en la tabla
						echo('<td>');
						if(key($row) == 'fotopersonal'){	//Si es la foto, convertirla en hipervinculo
							echo('<a href="../Files/'.$valor.'">Foto personal</a>');
						}
						else if(key($row) == 'sexo'){		//Si si el sexo, escribir la variable en el idioma correspondiente
							echo($strings[$valor]);
						}
						else{								//Si no, representar directamente los datos de la db
							echo($valor);
						}
						echo('</td>');
						next($row);
					}
					echo('<td>');
					echo('<form id="oculto" action="../Controllers/Edit_Controller.php" method="get" id="oculto" name="editButton">');
					echo('<input type="hidden" name="loginPassed" value="'.$row["login"].'">');
					echo('<input id="edit" type="submit" name="edit" value="" />');
					echo('</form>');
					echo('<form id="oculto" action="../Controllers/Showcurrent_Controller.php" method="get" id="oculto" name="viewButton">');
					echo('<input type="hidden" name="loginPassed" value="'.$row["login"].'">');
					echo('<input id="view" type="submit" name="view" value="" />');
					echo('</form>');
					echo('<form id="oculto" action="../Controllers/Delete_Controller.php" method="get" id="oculto" name="delButton">');
					echo('<input type="hidden" name="loginPassed" value="'.$row["login"].'">');
					echo('<input id="delete" type="submit" name="delete" value="" />');
					echo('</form>');
					echo('</td>');
					echo('</tr>');
				}
			echo('</table>');
			echo('</HTML>');
			}
		include '../Views/Footer.php'; 
		}
	}
?>