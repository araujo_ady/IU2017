
<?php
	session_start();
	
	include_once './Functions/Authentication.php';
	
	if (!IsAuthenticated()){									//El usuario no está autenticado
		header("Location:./Controllers/Login_Controller.php");
	}
	
	else{														//El usuario está autenticado
		header("Location:./Controllers/Index_Controller.php");
	}
?>