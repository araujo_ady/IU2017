<?php
/*
Archivo creado por wqinmz el 08/11/2017. Script controlador del acceso inicial a la web.
*/
	error_reporting(0);											//Ocultar reportes de errores y advertencias
	session_start();
    include_once '../Functions/Authentication.php';
	if (!IsAuthenticated()){			//Si el usuario no está autenticado, redireccionar a la vista de logeo.
		header("Location:../Controllers/Login_Controller.php");
	}
	else{								//Si lo está, cargar vista de bienvenida.
		include '../Views/Principal_View.php';
		new Principal();
	}
?>