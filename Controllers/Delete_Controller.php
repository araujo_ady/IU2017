<?php
/*
Archivo creado por wqinmz el 08/11/2017. Script controlador para formularios de edición de la tabla Usuario.
*/
	error_reporting(0);											//Ocultar reportes de errores y advertencias
	session_start();											//Inicio del sistema de sesiones.
	include_once '../Functions/Authentication.php';
	require_once '../Models/Usuario.php';						//Necesita usar el modelo de datos Usuario.php para conectarse a la base de datos.
	if (!IsAuthenticated()){									//Si el usuario no está identificado, redireccionar a login.
		header("Location:../Controllers/Login_Controller.php");
	}
	else{														//Si está identificado, cargar la vista.
		if(!isset($_POST['loginDel'])){							//Si no se le pasa ningún login parar borrar, cargar la vista
			if(!isset($_GET['loginPassed'])){					//Si no se le pasa ningún login para visualizar, cargar el usuario
				$profile = new Usuario(							//Variable Usuario para la búsqueda de la tupla
						$_SESSION['login'], 
						'%', 
						'%', 
						'%',
						'%', 
						'%',
						'%',
						'%', 
						'%', 
						'%'
						);
				$tupla = $profile->get();						//Almacena la tupla a visualizar
				include_once '../Views/Usuario_DELETE.php';		
				$edit = new Usuario_DELETE($tupla);				//Cargar la vista
			}
			else{												//Si se le pasa un login de visualización, cargar su tupla
				$profile = new Usuario(							//Variable para la búsqueda de la tupla a visualizar
						$_GET['loginPassed'], 
						'%', 
						'%', 
						'%',
						'%', 
						'%',
						'%',
						'%', 
						'%', 
						'%'
						);
			$tupla = $profile->get();							//Almacena la tupla en $tupla
			include_once '../Views/Usuario_DELETE.php';
			$edit = new Usuario_DELETE($tupla);					//Cargar la vista
			}
		}	
		else{													//Si se le pasa un login de borrado, borrar su tupla														
			$profile = new Usuario(
						$_POST['loginDel'], 
						'%', 
						'%', 
						'%',
						'%', 
						'%',
						'%',
						'%', 
						'%', 
						'%'
						);
			$mensaje = $profile->delete();
			include '../Views/MESSAGE.php';
			if($_POST['loginDel'] == $_SESSION['login']){		//Si el perfil borrado es el del propio usuario, desconectar
				new MESSAGE($mensaje, '../Functions/Desconectar.php');
			}
			else {												//Si no, sacar mensaje de resultado.
				new MESSAGE($mensaje, '../index.php');
			}
		}
	}
?>