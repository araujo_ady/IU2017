<?php
/*
Archivo creado por wqinmz el 08/11/2017. Script controlador para formularios de edición de la tabla Usuario.
*/
	error_reporting(0);											//Ocultar reportes de errores y advertencias
	session_start();											//Inicio del sistema de sesiones.
	include_once '../Functions/Authentication.php';
	require_once '../Models/Usuario.php';						//Necesita usar el modelo de datos Usuario.php para conectarse a la base de datos.
	if (!IsAuthenticated()){									//Si el usuario no está identificado, redireccionar a login.
		header("Location:../Controllers/Login_Controller.php");
	}
	else{														//Si está identificado, cargar la vista.
		if(!isset($_POST['login'])){							//Si no hay información de formulario
			if(!isset($_GET['loginPassed'])){						//Si no se le pasa ningún perfil para cargar, cargar el del usuario logeado
				$profile = new Usuario(
					$_SESSION['login'], 
					'%', 
					'%', 
					'%',
					'%', 
					'%',
					'%',
					'%', 
					'%', 
					'%'
					);
				$tupla = $profile->get();
				require_once '../Views/Usuario_EDIT.php';
				$edit = new Usuario_EDIT($tupla);
			}
			else{										//Si no, cargar la tupla del login que se le pasa
				$profile = new Usuario(
					$_GET['loginPassed'], 
					'%', 
					'%', 
					'%',
					'%', 
					'%',
					'%',
					'%', 
					'%', 
					'%'
					);
				$tupla = $profile->get();
				include_once '../Views/Usuario_EDIT.php';
				$edit = new Usuario_EDIT($tupla);
			}
		}
		else{								//Si hay información de formulario, llevar a cabo la edición.
			// Recibo los datos de la imagen
			$nombre_img = $_FILES['foto']['name'];
			$tipo = $_FILES['foto']['type'];
			$tamano = $_FILES['foto']['size'];
			
			//Si existe imagen y tiene un tamaño correcto
			if (($nombre_img == !NULL) && ($_FILES['foto']['size'] <= 200000)) 
			{
				if (($_FILES["foto"]["type"] == "image/jpeg") //Validación del tipo
   				|| ($_FILES["foto"]["type"] == "image/jpg")
   				|| ($_FILES["foto"]["type"] == "image/png"))
   				{
					// Ruta donde se guardarán las imágenes que subamos
					$directorio = '../Files/';
					// Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
					move_uploaded_file($_FILES['foto']['tmp_name'],$directorio.$nombre_img);
				}
				else{
					include_once '../Views/MESSAGE.php';
					new MESSAGE($strings['El formato del archivo es inválido'], '../Controllers/Insert_Controller.php');
				}
			}
			else 
			{
				if($nombre_img == !NULL){	//si existe la variable pero se pasa del tamaño permitido
					include_once '../Views/MESSAGE.php';
					new MESSAGE($strings['La imagen excede el tamaño máximo permitido.'], '../Controllers/Insert_Controller.php');
				}
			}
					
			$user = new Usuario(					//Variable donde se almacenan los nuevos datos del usuario obtenidos desde la vista de inserción (formulario POST).
				$_POST['login'], 
				$_POST['Password'], 
				$_POST['DNI'], 
				$_POST['nombre'],
				$_POST['apellidos'], 
				$_POST['telefono'],
				$_POST['email'],
				$_POST['nacimiento'], 
				$nombre_img, 
				$_POST['sexo']
				);
			$mensaje = $user->edit();
			include '../Views/MESSAGE.php';
			new MESSAGE($mensaje, '../index.php');
	}
}
?>