<?php
/*
Archivo creado por wqinmz el 08/11/2017. Script controlador para el login en el sistema.
*/
	error_reporting(0);											//Ocultar reportes de errores y advertencias
	session_start();															//Inicio de sesión
	if(!isset($_REQUEST['login']) && !(isset($_REQUEST['password']))){			//Si no hay información en los campos, mostrar la vista de login.
		include '../Views/Login_View.php';
		$login = new Login();
	}
	else{																		//Si hay información en los campos, intentar logear con las credenciales introducidas.
		require '../Models/Usuario.php';										//Se necesita el modelo Usuario.php para conectarse a la DB.
		$user = new Usuario(													//Nuevo usuario con solo datos de login y password para intentar logear.
		$_REQUEST['login'], 
		$_REQUEST['Password'], 
		'%', 
		'%',
		'%', 
		'%',
		'%',
		'%', 
		'%', 
		'%'
		);
		$respuesta = $user->login();											//Variable que almacena método de login definido en Usuario.php
		
		if ($respuesta == 'true'){												//Si la respuesta del logeo es true, definir el login de la session.
			session_start();
			$_SESSION['login'] = $_REQUEST['login'];
			header('Location:../index.php');									//Redireccionar a la página principal
		}
		else{																	//Si el proceso de login no finaliza correctamente
			include '../Views/MESSAGE.php';
			new MESSAGE($respuesta, './Login_Controller.php');
		}
	}
?>