<?php
/*
Archivo creado por wqinmz el 08/11/2017. Script controlador para formularios de edición de la tabla Usuario.
*/
	error_reporting(0);											//Desactivar notificaciones
	session_start();											//Inicio del sistema de sesiones.
	include_once '../Functions/Authentication.php';
	require_once '../Models/Usuario.php';						//Necesita usar el modelo de datos Usuario.php para conectarse a la base de datos.
	if (!IsAuthenticated()){									//Si el usuario no está identificado, redireccionar a login.
		header("Location:../Controllers/Login_Controller.php");
	}
	else{														//Si está identificado, cargar la vista.
		if(!isset($_GET['loginPassed'])){						//Si no se le pasa ningún login, cargar el del usuario
			$profile = new Usuario(								//Variable Usuario para la búsqueda del perfil a visualizar
					$_SESSION['login'], 
					'%', 
					'%', 
					'%',
					'%', 
					'%',
					'%',
					'%', 
					'%', 
					'%'
					);
			$tupla = $profile->get();							//Cargar la tupla con el login indicado en $profile
			include_once '../Views/Usuario_SHOWCURRENT.php';
			$edit = new Usuario_SHOWCURRENT($tupla);			//Cargar la vista
		}
		else{													//Si se le pasa un login, buscarlo en db y mostrarlo
			$profile = new Usuario(								//Variable Usuario para la búsqueda del perfil a visualizar
					$_GET['loginPassed'], 
					'%', 
					'%', 
					'%',
					'%', 
					'%',
					'%',
					'%', 
					'%', 
					'%'
					);
			$tupla = $profile->get();
			include_once '../Views/Usuario_SHOWCURRENT.php';
			$show = new Usuario_SHOWCURRENT($tupla);			//Mostrar la tupla
		}
	}
?>