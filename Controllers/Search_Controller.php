<?php
/*
Archivo creado por wqinmz el 08/11/2017. Script controlador para llevar a cabo búsquedas en la tabla Usuario.
*/
	error_reporting(0);											//Ocultar reportes de errores y advertencias
	session_start();
    include_once '../Functions/Authentication.php';

	if (!IsAuthenticated()){	//Si el usuario no está autenticado, redireccionar al logeo
		header("Location:../Controllers/Login_Controller.php");
	}
	else{		//Si lo está, iniciar las vistas
		if((!isset($_GET['login'])||!isset($_GET['Password'])||!isset($_GET['DNI'])||!isset($_GET['nombre'])||!isset($_GET['apellidos'])||
			!isset($_GET['telefono'])||!isset($_GET['email'])||!isset($_GET['nacimiento'])||!isset($_GET['foto'])||!isset($_GET['sexo']))){		//Si no hay ningún campo del formulario cubierto, cargar vista de formulario
			include '../Views/Usuario_SEARCH.php';
			$search = new Usuario_SEARCH();		//Vista de formulario
		}
		else{	//Si no, iniciar búsqueda
			require '../Models/Usuario.php';
			$user = new Usuario(		//Variable Usuario para llevar a cabo la búsqueda
				"%".$_GET['login']."%", 
				"%".$_GET['Password']."%", 
				"%".$_GET['DNI']."%", 
				"%".$_GET['nombre']."%",
				"%".$_GET['apellidos']."%", 
				"%".$_GET['telefono']."%",
				"%".$_GET['email']."%",
				"%".$_GET['nacimiento']."%", 
				"%".$_GET['foto']."%", 
				"%".$_GET['sexo']."%"
			);
			$results = $user->get();
			if(count($results)>0){		//Si se encuentran resultados, mostrar showall
				include '../Views/Usuario_SHOWALL.php';
				$table = new Showall($results, array_keys($results[0]));
			} 
			else{						//Si no, sacar mensaje por pantalla
				include '../Views/MESSAGE.php';
				new MESSAGE('No se han obtenido resultados', '../index.php');
			}
		}
	}
?>